require 'httparty'
require 'pry'

require_relative './models/exchange_evaluator'
require_relative './templates/exchange_evaluator_template'

response = HTTParty.get('https://api.bankofthecommons.coop/exchange/v1/ticker/eur')
data =  JSON.parse(response.body)["data"]
cot_actual = data["BTCxEUR"]

eurs = `ledger bal allocation -H -X EUR`.to_f
bits = `ledger bal allocation`.to_f

cot = eurs/bits*1000

puts ExchangeEvaluatorTemplate.new(
  ExchangeEvaluator.new(
    market_exchange: cot_actual,
    cost: cot,
    fees: 0.03
  )
).to_s


require 'pry'
require_relative '../models/exchange_evaluator'

describe ExchangeEvaluator, type: :model do
  context 'when the market exchange is lower than the cost' do
    let(:exchange_evaluator) do
      ExchangeEvaluator.new(
        market_exchange: 9030,
        cost: 9430
      )
    end

    it 'we should buy' do
      expect(exchange_evaluator.buy?).to be_truthy
    end

    it 'we cant sell' do
      expect(exchange_evaluator.sell?).to be_falsey
    end

    it 'we don\'t wait' do
      expect(exchange_evaluator.wait?).to be_falsey
    end

    it 'returns the action' do
      expect(exchange_evaluator.action).to eq('BUY')
    end
  end

  context 'when the market exchange is the same to the cost' do
    let(:exchange_evaluator) do
      ExchangeEvaluator.new(
        market_exchange: 9000,
        cost: 9000
      )
    end

    it 'we should wait' do
      expect(exchange_evaluator.wait?).to be_truthy
    end

    it 'we shouldn\'t buy' do
      expect(exchange_evaluator.buy?).to be_falsey
    end

    it 'we cant sell' do
      expect(exchange_evaluator.sell?).to be_falsey
    end

    it 'returns the action' do
      expect(exchange_evaluator.action).to eq('WAIT')
    end
  end

  context 'when the market exchange is higher than the cost' do
    let(:exchange_evaluator) do
      ExchangeEvaluator.new(
        market_exchange: 9600,
        cost: 9000
      )
    end

    it 'we should wait' do
      expect(exchange_evaluator.wait?).to be_falsey
    end

    it 'we shouldn\'t buy' do
      expect(exchange_evaluator.buy?).to be_falsey
    end

    it 'we cant sell' do
      expect(exchange_evaluator.sell?).to be_truthy
    end

    it 'returns the action' do
      expect(exchange_evaluator.action).to eq('SELL')
    end
  end

  context 'with a gain for selling' do
    it 'we should wait until the exchange is higher than gain percentage' do
      expect(
        ExchangeEvaluator.new(
          market_exchange: 9200,
          cost: 9000,
          gain: 0.04
        ).wait?
      ).to be_truthy
    end

    it 'gain wont affect buying'do
      expect(
        ExchangeEvaluator.new(
          market_exchange: 8900,
          cost: 9000,
          gain: 0.04
        ).buy?
      ).to be_truthy
    end
  end

  context 'there is a cost on every transcation' do
    it 'we should wait' do
      expect(
        ExchangeEvaluator.new(
          market_exchange: 8900,
          cost: 9000,
          fees: 0.02
        ).wait?
      ).to be_truthy
    end

    it 'we should wait' do
      expect(
        ExchangeEvaluator.new(
          market_exchange: 9100,
          cost: 9000,
          fees: 0.02
        ).wait?
      ).to be_truthy
    end
  end

  describe 'the gaining results' do
    it 'returns the current gainings' do
      expect(
        ExchangeEvaluator.new(
          market_exchange: 9100,
          cost: 9000,
          fees: 0.02
        ).gaining
      ).to eq(9100/9000-0.02)
    end
  end
end

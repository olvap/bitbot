class ExchangeEvaluatorTemplate
  attr_reader :exchange_evaluator

  def initialize(exchange_evaluator)
    @exchange_evaluator = exchange_evaluator
  end

  def to_s
    <<~TEXT
    Marke Value: #{exchange_evaluator.market_exchange}
    +1% #{exchange_evaluator.market_exchange * 1.01}
    Cost: #{exchange_evaluator.cost}
    Action: #{exchange_evaluator.action}
    Gaining: #{exchange_evaluator.gaining}
    TEXT
  end
end

#!/usr/bin/env ruby

require 'httparty'
require 'json'
require 'pry'

class Exchange
  attr_accessor :rates

  def initialize
    url = URI.parse("http://data.fixer.io/api/latest?access_key=bdcce73e58b41b6d585cb529a35ef2f6")
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    response = res.body
    currencies = ['ARS', 'USD', 'BTC']

    self.rates = JSON.parse(response)['rates'].select { |currency, _| currencies.include? currency }
  end

  def ars_to_eur(ars)
    ars / rates['ARS']
  end

  def ars_to_btc(ars)
    ars_to_eur(ars) / data['BTCxEUR']
  end

  def btc_to_eur(btc)
    btc * data['BTCxEUR']
  end

  private

  def data
    return @data if @data
    response = HTTParty.get('https://api.bankofthecommons.coop/exchange/v1/ticker/eur')
    @data =  JSON.parse(response.body)["data"]
  end
end

def ars_to_btc(ars)
  Exchange.new.ars_to_btc(ars)
end

def btc_to_eur(btc)
  Exchange.new.btc_to_eur(btc)
end

binding.pry


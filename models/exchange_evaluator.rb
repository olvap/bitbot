class ExchangeEvaluator
  attr_reader :market_exchange, :cost, :fees, :gain

  def initialize(market_exchange:, cost:, fees: 0, gain: 0)
    @market_exchange = market_exchange
    @cost = cost
    @fees = fees
    @gain = gain
  end

  def action
    return 'BUY' if buy?
    return 'SELL' if sell?
    'WAIT'
  end

  def buy?
    market_exchange < cost / (1 + fees)
  end

  def sell?
    market_exchange > cost * (1 + fees + gain)
  end

  def wait?
    !buy? && !sell?
  end

  def gaining
    market_exchange / cost - fees
  end
end
